//
//  Add_Menu_ViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/25/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Add_Menu_ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func Goto_nxtview(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Category_listViewController") as! Category_listViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
