//
//  Category_details_listViewController.swift
//  IQKeyboardManager
//
//  Created by Appentus Technologies on 11/20/19.
//

import UIKit

class Category_details_listViewController: UIViewController {
    //MARK: outlet
    
    @IBOutlet weak var view_upload_image: UIView!
    @IBOutlet weak var view_item_category: UIView!
    @IBOutlet weak var view_item_size: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_item_category.layer.cornerRadius = 8
        view_item_category.layer.borderWidth = 1
        view_item_category.layer.borderColor = UIColor.lightGray.cgColor
       
        view_item_size.layer.cornerRadius = 8
        view_item_size.layer.borderWidth = 1
        view_item_size.layer.borderColor = UIColor.lightGray.cgColor
        
        view_upload_image.layer.cornerRadius = 8
        view_upload_image.layer.borderWidth = 1
        view_upload_image.layer.borderColor = UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btn_goto_nextVC(_ sender: UIButton) {
   
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Post_order_ViewController") as! Post_order_ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

