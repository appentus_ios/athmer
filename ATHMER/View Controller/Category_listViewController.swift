//
//  Category_listViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/20/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Category_listViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: variable

    //MARK: All outlet
    @IBOutlet weak var tbl_Category_list: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tbl_Category_list.delegate = self
        tbl_Category_list.dataSource = self
           
    }
    
    //MARK: function
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Category_ViewCell", for: indexPath) as! Category_ViewCell

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Category_details_listViewController") as! Category_details_listViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btn_goto_nextVC(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Category_details_listViewController") as! Category_details_listViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
