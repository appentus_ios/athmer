//
//  Catogory_detailsViewController.swift
//  
//
//  Created by Appentus Technologies on 11/20/19.
//

import UIKit

class Catogory_detailsViewController: UIViewController {
    //MARK: All outlets
    
    @IBOutlet weak var Circle_view: UIView!
    @IBOutlet weak var Item_category_view: UIView!
    @IBOutlet weak var Item_size_view: UIView!
    @IBOutlet weak var txt_Item_name: UITextField!
    @IBOutlet weak var txt_Item_category: UITextField!
    @IBOutlet weak var txt_item_size: UITextField!
    @IBOutlet weak var txt_Item_price: UITextField!
    @IBOutlet weak var txt_item_offerprice: UITextField!
    @IBOutlet weak var txt_Item_discription: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Circle_view.layer.borderColor = UIColor.gray.cgColor
        Circle_view.layer.borderWidth = 2.0
        Circle_view.layer.cornerRadius = 10.0
        // Do any additional setup after loading the view.
        
        func_set_border_TF(txt_Item_name)
        func_set_border_TF(Item_category_view)
        func_set_border_TF(Item_size_view)
        func_set_border_TF(txt_Item_price)
        func_set_border_TF(txt_item_offerprice)
        func_set_border_TF(txt_Item_discription)
    }
    // MARK: Custom function
    
    func func_set_border_TF(_ view:UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
    }
    

}
