//
//  Choose_Month_ViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/21/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Choose_Month_ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    //MARK: variable
    let Month_Name = ["Sep-2019", "Aug-2019", "jul-2019", "Jun-2019", "May-2019","April-2019","Mar-2019","Feb-2019","Dec-2018","Nov-2018","Oct-2018","Sep-2018"]
    let cellReuseIdentifier = "cell"

    //MARK:All Outlet
    @IBOutlet weak var tbl_Choose_Month: UITableView!
    @IBOutlet weak var month_popup_top: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbl_Choose_Month.delegate = self
        tbl_Choose_Month.dataSource = self
        // Do any additional setup after loading the view.
//        
//        let iPhone_Screen_Size = UIScreen.main.nativeBounds.height
//        let height_Screen = self.view.bounds.height
//        
//        if iPhone_Screen_Size == 1136 {
//            view_geight.constant = 600
//        } else if iPhone_Screen_Size == 1334 {
//            view_geight.constant = height_Screen-44
//        } else if iPhone_Screen_Size == 1792 {
//            view_geight.constant = height_Screen-88
//        } else if iPhone_Screen_Size == 1920 || iPhone_Screen_Size == 2208 {
//            view_geight.constant = height_Screen-44
//        } else if iPhone_Screen_Size == 2436 {
//            view_geight.constant = height_Screen-88
//        } else if iPhone_Screen_Size == 2688 {
//            view_geight.constant = height_Screen-88
//        }
//        
    
    }
    
    //MARK: All Custom function
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
        
    }
    
    //MARK: function
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Month_Name.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Choose_Month_ViewCell
        
        // set the text from the data model
        cell.Lbl_Month.text = self.Month_Name[indexPath.row]
        return cell
    }

    @IBAction func btn_close(_ sender: UIButton) {
        self.view.removeFromSuperview()
    }

}
