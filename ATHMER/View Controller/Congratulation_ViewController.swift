//
//  Congratulation_ViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/26/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Congratulation_ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Action
    
    @IBAction func btn_Goto_next_Vc(_ sender: UIButton) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "Sign_In_ViewController") as! Sign_In_ViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
  

}
