//
//  Create_Password_ViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/19/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Create_Password_ViewController: UIViewController,UITextFieldDelegate {
    
    //    MARK: outlet btn and txtfiled
        @IBOutlet weak var txt_Password: UITextField!
        @IBOutlet weak var txt_retype_Password: UITextField!
        @IBOutlet weak var btnhide_show: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_retype_Password.delegate = self
        txt_Password.delegate = self
        //txt_Emailaddress.layer.masksToBounds = true
        txt_Password.layer.borderColor = UIColor.gray.cgColor
        txt_Password.layer.borderWidth = 1.0
        txt_Password.layer.cornerRadius = 8.0
        
        //txt_Password.layer.masksToBounds = true
        txt_retype_Password.layer.borderColor = UIColor.gray.cgColor
        txt_retype_Password.layer.borderWidth = 1.0
        txt_retype_Password.layer.cornerRadius = 8.0
        // Do any additional setup after loading the view.
        
    }
    //MARK: custum function
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if txt_Password == textField {
            txt_Password.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
        }
        
        if txt_retype_Password == textField {
            txt_retype_Password.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !txt_Password.text!.isEmpty {
                txt_Password.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
        } else {
            txt_Password.layer.borderColor = UIColor.gray.cgColor
        }
        
        if !txt_retype_Password.text!.isEmpty {
            txt_retype_Password.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
        } else {
            txt_retype_Password.layer.borderColor = UIColor.gray.cgColor
        }
        
    }
    
    
    
    
    
     //MARK: custum function
    @IBAction func btn_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Goto_nxtview(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Sign_In_ViewController") as! Sign_In_ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnhide_show(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txt_retype_Password.isSecureTextEntry = !sender.isSelected
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
