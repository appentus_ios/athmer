//
//  Forget_Password_ViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/19/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Forget_Password_ViewController: UIViewController,UITextFieldDelegate {
    
    //    MARK: outlet btn and txtfiled
    
    @IBOutlet weak var txt_Password: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_Password.delegate = self
        txt_Password.layer.borderColor = UIColor.gray.cgColor
        txt_Password.layer.borderWidth = 1.0
        txt_Password.layer.cornerRadius = 8.0
        
//        let iPhone_Screen_Size = UIScreen.main.nativeBounds.height
//        let height_Screen = self.view.bounds.height
//        
//        if iPhone_Screen_Size == 1136 {
//            view_geight.constant = 600
//        } else if iPhone_Screen_Size == 1334 {
//            view_geight.constant = height_Screen-44
//        } else if iPhone_Screen_Size == 1792 {
//            view_geight.constant = height_Screen-88
//        } else if iPhone_Screen_Size == 1920 || iPhone_Screen_Size == 2208 {
//            view_geight.constant = height_Screen-44
//        } else if iPhone_Screen_Size == 2436 {
//            view_geight.constant = height_Screen-88
//        } else if iPhone_Screen_Size == 2688 {
//            view_geight.constant = height_Screen-88
//        }
        
        // Do any additional setup after loading the view.
    }
    
      // MARk: btn action
    @IBAction func btn_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Go_to_nxtview(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Otp_ViewController") as! Otp_ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
