//
//  Get_update_popupViewController.swift
//  IQKeyboardManager
//
//  Created by Appentus Technologies on 11/20/19.
//

import UIKit

class Get_update_popupViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }
        //MARK: Action
    
    @IBAction func btn_back(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    @IBAction func Goto_nxtview(_ sender: UIButton) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "Find_Loction_popupViewController") as! Find_Loction_popupViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
