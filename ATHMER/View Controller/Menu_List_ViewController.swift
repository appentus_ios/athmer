//
//  Menu_List_ViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/29/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit
import PVSwitch

class Menu_List_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    // MARK: outlet
    
    @IBOutlet weak var tbl_Menu_list: UITableView!
    @IBOutlet weak var view_offer_view: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbl_Menu_list.delegate = self
        tbl_Menu_list.dataSource = self
        view_offer_view.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_offer(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Menu_OffersViewController") as! Menu_OffersViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_edit_items(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Menu_Save_Change_ViewController") as! Menu_Save_Change_ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_delete_item(_ sender: UIButton) {
        
    }
    // MARK: Custom Function
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Menu_List_ViewCell", for: indexPath) as! Menu_List_ViewCell
        //        cell.item_name.text = self.item_name1[indexPath.row]
        //        cell.item_image.image = self.item_image1[indexPath.row]
        return cell
    }
   
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        <#code#>
//    }
}
