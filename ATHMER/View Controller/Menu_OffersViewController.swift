//
//  Menu_OffersViewController.swift
//  ATHMER
//
//  Created by appentus on 12/4/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Menu_OffersViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tbl_Menu_Offer: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tbl_Menu_Offer.delegate = self
        tbl_Menu_Offer.dataSource = self

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Menu_Offers_ViewCell", for: indexPath) as! Menu_Offers_ViewCell
        //        cell.item_name.text = self.item_name1[indexPath.row]
        //        cell.item_image.image = self.item_image1[indexPath.row]
        return cell
    }


}
