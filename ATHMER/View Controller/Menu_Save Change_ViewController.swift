//
//  Menu_Save Change_ViewController.swift
//  ATHMER
//
//  Created by appentus on 12/4/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Menu_Save_Change_ViewController: UIViewController {
    //MARK: Outlet
    
    @IBOutlet weak var view_item_category: UIView!
    @IBOutlet weak var view_item_size: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        view_item_category.layer.cornerRadius = 8
        view_item_category.layer.borderWidth = 1
        view_item_category.layer.borderColor = UIColor.lightGray.cgColor
        view_item_size.layer.cornerRadius = 8
        view_item_size.layer.borderWidth = 1
        view_item_size.layer.borderColor = UIColor.lightGray.cgColor
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
