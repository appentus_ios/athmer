//  New_OrderViewController.swift
//  ATHMER
//  Created by Appentus Technologies on 11/20/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.


import UIKit


class New_OrderViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,Delegate_Random {
    //MARK: all variable
    let lbl_orderno = ["Order #7852 7894 Today,12 Sep 2019,12.45 PM","order#45612","order#45612","order#45612","order#45612","order#45612"]
    let lbl_item_name = ["italian pizza", "Cheese Pizza", "italian pizza", "Cheese Pizza", "Cheese Pizza","Cheese Pizza"]
    let lbl_item_size = ["small double cheese","small double cheese","small double cheese","small double cheese","small double cheese","small double cheese"]
    let lbl_item_quantity = ["2","2","2","2","2","2"]
    let lbl_item_total = ["$52.00","$52.00","$52.00","$52.00","$52.00","$52.00"]
    
    //MARK: All Outlet
    @IBOutlet weak var tbl_Orderview: UITableView!
    @IBOutlet weak var tbl_new_item: UITableView!
    @IBOutlet weak var view_empty_new_order: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbl_Orderview.delegate = self
        tbl_Orderview.dataSource = self
        
        tbl_Orderview.isHidden = true
        view_empty_new_order.isHidden = false
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
            self.tbl_Orderview.isHidden = false
            self.view_empty_new_order.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(190+(44*random_numbers))
    }
    
    //MARK: function
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.lbl_orderno.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! New_OrderViewCell
        
        cell.tag = indexPath.row
        cell.delegate = self
        
        // set the text from the data model
//        cell.lbl_orderno.text = self.lbl_orderno[indexPath.row]
//        cell.lbl_item_name.text = self.lbl_item_name[indexPath.row]
//        cell.lbl_item_size.text = self.lbl_item_size[indexPath.row]
//        cell.lbl_item_quantity.text = self.lbl_item_quantity[indexPath.row]
//        cell.lbl_item_total.text = self.lbl_item_total[indexPath.row]
        
        //cell.layer.shadowOffset = CGSizeMake(0, 0)
        
        return cell
    }
    
    func func_Delegate_Random(_ cell: New_OrderViewCell) {
        tbl_Orderview.reloadRows(at: [IndexPath (row: cell.tag, section: 0)], with:.automatic)
    }
    @IBAction func btn_Accept_order(_ sender: UIButton) {
    
    }
    
}
