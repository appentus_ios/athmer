//
//  Ongoing_Details_ViewController.swift
//  ATHMER
//
//  Created by appentus on 12/3/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Ongoing_Details_ViewController: UIViewController {

    @IBOutlet weak var Tbl_Ongoing_Details: UITableView!
    @IBOutlet weak var height_tbl_view: NSLayoutConstraint!
    @IBOutlet weak var lbl_item_total_rate: UILabel!
    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var lbl_user_name: UILabel!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var btn_phone_no: UIButton!
    @IBOutlet weak var lbl_delivery_boy_phno: UILabel!
    @IBOutlet weak var lbl_delivery_boy_name: UILabel!
    @IBOutlet weak var lbl_order_preapring: UILabel!
    @IBOutlet weak var btn_cancel_order: Custom_Button_Border!
    @IBOutlet weak var lbl_order_no: UILabel!
    @IBOutlet weak var lbl_order_date_time: UILabel!
    @IBOutlet weak var btn_cod: Custom_Button_Border!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        height_tbl_view.constant = 50*5
    }
    
}

extension Ongoing_Details_ViewController: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "Ongoing_details_itemViewCell", for: indexPath) as! Ongoing_details_itemViewCell
        return cell2
    }
}
