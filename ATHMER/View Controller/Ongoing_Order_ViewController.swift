//
//  Ongoing_Order_ViewController.swift
//  ATHMER
//
//  Created by appentus on 12/3/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Ongoing_Order_ViewController: UIViewController{
    //MARK: variable
    let lbl_orderno = ["Order #7852 7894 Today,12 Sep 2019,12.45 PM","order#45612","order#45612","order#45612","order#45612","order#45612"]
   
     //MARK: Outlet
    @IBOutlet weak var tbl_Ongoing_order: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    tbl_Ongoing_order.delegate = self
    tbl_Ongoing_order.dataSource = self
    }
    
}

extension Ongoing_Order_ViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(240+(48*5))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.lbl_orderno.count

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Ongoing_order_ViewCell
        return cell
    }
}
