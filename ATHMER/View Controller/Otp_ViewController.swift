//
//  Otp_ViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/19/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Otp_ViewController: UIViewController {
    //    MARK: txtfiled
    @IBOutlet weak var txt_OTP_1: UITextField!
    @IBOutlet weak var txt_OTP_2: UITextField!
    @IBOutlet weak var txt_OTP_3: UITextField!
    @IBOutlet weak var txt_OTP_4: UITextField!
    @IBOutlet weak var lbl_otp_timer: UILabel!
    
    // MARK: VAriable
    
       override func viewDidLoad() {
       super.viewDidLoad()
                   // Do any additional setup after loading the view.

    }
    // MARk: btn action
    @IBAction func btn_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func Goto_nxtview(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Create_Password_ViewController") as! Create_Password_ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension Otp_ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let ACCEPTABLE_CHARACTERS = "0123456789"
        
        if textField == txt_OTP_1 {
            if txt_OTP_1.text!.count > 0 {
                if string == "" {
                    return true
                } else {
                    txt_OTP_2.becomeFirstResponder()
                    return false
                }
            } else {
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                
                return (string == filtered)
            }
        } else if textField == txt_OTP_2 {
            if txt_OTP_2.text!.count > 0 {
                if string == "" {
                    return true
                } else {
                    txt_OTP_3.becomeFirstResponder()
                    return false
                }
            } else {
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                
                return (string == filtered)
            }
        } else if textField == txt_OTP_3 {
            if txt_OTP_3.text!.count > 0 {
                if string == "" {
                    return true
                } else {
                    txt_OTP_4.becomeFirstResponder()
                    return false
                }
            } else {
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                
                return (string == filtered)
            }
        } else if textField == txt_OTP_4 {
            if txt_OTP_4.text!.count > 0 {
                if string == "" {
                    return true
                } else {
                    self.view.endEditing(true)
                    return false
                }
            } else {
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                
                return (string == filtered)
            }
        } else {
            return true
        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if txt_OTP_1 == textField {
            txt_OTP_1.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
        }
        
        if txt_OTP_2 == textField {
            txt_OTP_2.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
        }
        
        if txt_OTP_3 == textField {
            txt_OTP_3.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
        }
        
        if txt_OTP_4 == textField {
            txt_OTP_4.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == txt_OTP_1{
            if !txt_OTP_1.text!.isEmpty {
                txt_OTP_1.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
//            }
        } else {
            txt_OTP_1.layer.borderColor = UIColor .gray.cgColor
        }
        
//        if textField == txt_OTP_2 {
            if !txt_OTP_2.text!.isEmpty {
                txt_OTP_2.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
//            }
        } else {
            txt_OTP_2.layer.borderColor = UIColor .gray.cgColor
        }
        
//        if textField == txt_OTP_3{
            if !txt_OTP_3.text!.isEmpty {
                txt_OTP_3.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
//            }
        } else {
            txt_OTP_3.layer.borderColor = UIColor .gray.cgColor
        }
        
//        if textField == txt_OTP_3 {
            if !txt_OTP_3.text!.isEmpty {
                txt_OTP_3.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
//            }
        } else {
            txt_OTP_4.layer.borderColor = UIColor .gray.cgColor
        }
        
    }
    
    
}
