//
//  Our_Orders_ViewController.swift
//  ATHMER
//
//  Created by appentus on 12/4/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Our_Orders_ViewController: UIViewController {
    //MARK:Outlet
    var xPos: CGFloat = 0
    let horizontalBarView = UIView()
    
    @IBOutlet weak var switch_btn: UISwitch!
    @IBOutlet weak var view_button_container: UIView!
    
    @IBOutlet weak var new_order_btn: UIButton!
    @IBOutlet weak var ongoing_btn: UIButton!
    @IBOutlet weak var past_order_btn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch_btn.transform = CGAffineTransform(scaleX: 0.60, y: 0.60)
        setupHorizontalBar()
        NotificationCenter.default.addObserver(self, selector: #selector(self.get_offset_set_view(_:)), name: Notification.Name("get_offset_set_view"), object: nil)
        set_btn_select_state(tag: 1)
    }
    
    @IBAction func btn_order(_ sender: UIButton) {
        self.set_btn_select_state(tag: sender.tag)
        NotificationCenter.default.post(name: Notification.Name("move_by_buttons"), object: sender.tag)
        switch sender.tag {
        case 1:
            NotificationCenter.default.post(name: Notification.Name("get_offset_set_view"), object: 0)
            
        case 2:
            NotificationCenter.default.post(name: Notification.Name("get_offset_set_view"), object: UIScreen.main.bounds.width/3)
            
        case 3:
            NotificationCenter.default.post(name: Notification.Name("get_offset_set_view"), object: (UIScreen.main.bounds.width/3)*2)
            
        default:
            break
        }
    }
    func setupHorizontalBar() {
        horizontalBarView.backgroundColor = UIColor(red: 4.0/255, green: 132.0/255, blue: 63.0/255,alpha: 1.0)
        horizontalBarView.translatesAutoresizingMaskIntoConstraints = false
        horizontalBarView.frame = CGRect(x: xPos, y: 39, width: view_button_container.frame.width/3, height: 1)
        self.view_button_container.addSubview(horizontalBarView)
    }
    
    func set_btn_select_state(tag: Int) {
        
        switch tag {
        case 1:
            new_order_btn.setTitleColor(UIColor.black, for: .normal)
            ongoing_btn.setTitleColor(UIColor.lightGray, for: .normal)
            past_order_btn.setTitleColor(UIColor.lightGray, for: .normal)
            
        case 2:
            ongoing_btn.setTitleColor(UIColor.black, for: .normal)
            new_order_btn.setTitleColor(UIColor.lightGray, for: .normal)
            past_order_btn.setTitleColor(UIColor.lightGray, for: .normal)
        case 3:
            past_order_btn.setTitleColor(UIColor.black, for: .normal)
            ongoing_btn.setTitleColor(UIColor.lightGray, for: .normal)
            new_order_btn.setTitleColor(UIColor.lightGray, for: .normal)
        default:
            break
        }
       
    }
    
    @objc func get_offset_set_view(_ sender: NSNotification){
        let val = sender.object as! CGFloat
        xPos = val
        print(xPos)
        self.horizontalBarView.removeFromSuperview()
        UIView.animate(withDuration: 0.15, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.setupHorizontalBar()
            if self.xPos == 0.0{
                self.set_btn_select_state(tag: 1)
            }else if self.xPos == UIScreen.main.bounds.width/3{
                self.set_btn_select_state(tag: 2)
            }else{
               self.set_btn_select_state(tag: 3)
            }
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
