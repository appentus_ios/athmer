//
//  Post_order_ViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/21/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Post_order_ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    // MARK Outlet
    
    @IBOutlet weak var tbl_Post_Order: UITableView!
    @IBOutlet weak var view_main_post_order: UIView!
    @IBOutlet weak var btn_orderdelivry: UIButton!
    @IBOutlet weak var btn_cancel_delevry: UIButton!
    @IBOutlet weak var bnt_paymnt_cod: UIButton!
    @IBOutlet weak var btn_paymnt_paid: UIButton!
    
    @IBOutlet weak var height_filterpopup: NSLayoutConstraint!
    @IBOutlet weak var view_main: UIView!
    @IBOutlet weak var view_popup_filter: UIView!
    
    //MARK: variable
    
    let cellReuseIdentifier = "cell"
    let order_no = ["Order #45627494", "Order #45627494", "Order #45627494", "Order #45627494", "Order #45627494","Order #45627494"]
    let Item_rate = ["$ 52.00","$ 58.00","$ 68.00","$ 42.00","$ 52.00","$ 68.00"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.height_filterpopup.constant = 0
        self.view_main_post_order.isHidden = true
        self.view_popup_filter.isHidden = true
        
        tbl_Post_Order.delegate = self
        tbl_Post_Order.dataSource = self
        self.btn_orderdelivry.setImage(UIImage(named: "Unselect.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
        self.btn_orderdelivry.setImage(UIImage(named: "selectfull.png")?.withRenderingMode(.alwaysOriginal), for: .selected)
        
        self.btn_cancel_delevry.setImage(UIImage(named: "Unselect.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
        self.btn_cancel_delevry.setImage(UIImage(named: "selectfull.png")?.withRenderingMode(.alwaysOriginal), for: .selected)
        
        self.btn_paymnt_paid.setImage(UIImage(named: "Unselect.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
        self.btn_paymnt_paid.setImage(UIImage(named: "selectfull.png")?.withRenderingMode(.alwaysOriginal), for: .selected)
       
        self.bnt_paymnt_cod.setImage(UIImage(named: "Unselect.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
        self.bnt_paymnt_cod.setImage(UIImage(named: "selectfull.png")?.withRenderingMode(.alwaysOriginal), for: .selected)
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    //MARK: function
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return self.order_no.count
        return 15
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Post_order_TableViewCell
        
        // set the text from the data model
        //cell.lbl_Order_no.text = self.order_no[indexPath.row]
    
        //cell.lbl_item_rate.text = self.Item_rate[indexPath.row]
        
        return cell
    }

    // MARK: - Action
    
    @IBAction func btn_order_delevry(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btn_cancel_delevry(_ sender: UIButton) {
     sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btn_paymnt_cod(_ sender: UIButton) {
      sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btn_paymnt_paid(_ sender: UIButton) {
     sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btn_Filter_popup(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5) {
            self.view_popup_filter.isHidden = false
            self.view_main_post_order.isHidden = false
            self.height_filterpopup.constant = 330
            self.view.setNeedsLayout()
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btn_closepopup(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5) {
            
            self.height_filterpopup.constant = 0.0
            
            self.view.setNeedsLayout()
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.4) {
            self.view_popup_filter.isHidden = true
            self.view_main_post_order.isHidden = true
        }
    }
    
    @IBAction func btn_month(_ sender: UIButton) {
        let month_VC = self.storyboard?.instantiateViewController(withIdentifier: "Choose_Month_ViewController") as! Choose_Month_ViewController
        self.addChild(month_VC)
        self.view.addSubview(month_VC.view)
    }
    
}
