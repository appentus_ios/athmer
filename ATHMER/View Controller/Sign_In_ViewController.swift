//
//  Sign_In_ViewController.swift
//  IQKeyboardManager
//
//  Created by Appentus Technologies on 11/19/19.
//

import UIKit

class Sign_In_ViewController: UIViewController,UITextFieldDelegate {
    
    //    MARK: outlet btn and txtfiled
    
    @IBOutlet weak var txt_Emailaddress: UITextField!
    @IBOutlet weak var txt_Password: UITextField!
    @IBOutlet weak var btnhide_show: UIButton!
   

    
    // MARK: variable
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var btnhide_show = true
        
        txt_Password.delegate = self
        txt_Emailaddress.delegate = self
        //txt_Emailaddress.layer.masksToBounds = true
        txt_Emailaddress.layer.borderColor = UIColor.gray.cgColor
        txt_Emailaddress.layer.borderWidth = 1.0
        txt_Emailaddress.layer.cornerRadius = 8.0
        
        //txt_Password.layer.masksToBounds = true
        txt_Password.layer.borderColor = UIColor.gray.cgColor
        txt_Password.layer.borderWidth = 1.0
        txt_Password.layer.cornerRadius = 8.0
        
//
//        self.btnhide_show.setImage(UIImage(named: "hidepassword")?.withRenderingMode(.alwaysOriginal), for: .normal)
//        self.btnhide_show.setImage(UIImage(named: "showpassword")?.withRenderingMode(.alwaysOriginal), for: .selected)
//
        // Do any additional setup after loading the view.
        
        func_set_border_TF(txt_Emailaddress)
        func_set_border_TF(txt_Password)
    }
    
    // MARK: custon function
    func func_set_border_TF(_ view:UIView) {
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if txt_Emailaddress == textField {
            txt_Emailaddress.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
        }
        if txt_Password == textField {
            txt_Password.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == txt_Emailaddress{
            if !txt_Emailaddress.text!.isEmpty {
                txt_Emailaddress.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
//            }
        }  else {
            txt_Emailaddress.layer.borderColor = UIColor .gray.cgColor
        }
        
//        if textField == txt_Password {
            if !txt_Password.text!.isEmpty {
                txt_Password.layer.borderColor = hexStringToUIColor(hex: "04843F").cgColor
//            }
        } else {
            txt_Password.layer.borderColor = UIColor .gray.cgColor
        }
        
    }
    
    // MARk: btn action
    @IBAction func btn_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_show_hide_Pwd(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        txt_Password.isSecureTextEntry = !sender.isSelected
    }
    
    
    @IBAction func btn_keep_me_login(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
   
    @IBAction func btn_gotoforgetpasword(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Forget_Password_ViewController") as! Forget_Password_ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Goto_Addmenu(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Add_Menu_ViewController") as! Add_Menu_ViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btn_goto_signup(_ sender: UIButton) {
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Sign_Up_ViewController") as!Sign_Up_ViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
