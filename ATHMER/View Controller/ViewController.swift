//
//  ViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/18/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func Goto_nxtview(_ sender: UIButton) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "Sign_In_ViewController") as! Sign_In_ViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func Goto_signupview(_ sender: UIButton) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "Sign_Up_ViewController") as! Sign_Up_ViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
}

