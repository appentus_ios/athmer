//
//  Waiting_Approval_ViewController.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/26/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Waiting_Approval_ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0) {
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "Congratulation_ViewController") as! Congratulation_ViewController
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }
}
