//
//  page.swift
//  FantaSwype
//
//  Created by Ayush Pathak on 28/11/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import Foundation
import UIKit

class page: UIPageViewController , UIPageViewControllerDelegate, UIPageViewControllerDataSource , UIScrollViewDelegate{
    
    
    var pages = [UIViewController]()
    var curr = Int()
    var current = Int()
    
    var last_x : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        let p1: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "New_OrderViewController")
        let p2: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "Ongoing_Order_ViewController")
        let p3: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "Post_order_ViewController")
        
        pages.append(p1)
        pages.append(p2)
        pages.append(p3)
        
        setViewControllers([p1], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.move_by_buttons(_:)), name: Notification.Name("move_by_buttons"), object: nil)

    }
    
    @objc func move_by_buttons(_ sender:NSNotification) {
        let index = sender.object as! Int
        self.setViewControllers([pages[index-1]], direction: .forward, animated: true, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController)-> UIViewController? {
        
        curr = pages.index(of: viewController)!
        
        // if you prefer to NOT scroll circularly, simply add here:
        if curr == 0 { return nil }
        let prev = abs((curr - 1) % pages.count)
        return pages[prev]
        
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController)-> UIViewController? {
        
        let current = pages.firstIndex(of: viewController)!
        // if you prefer to NOT scroll circularly, simply add here:
        if current == (pages.count - 1) { return nil }
        let nxt = abs((current + 1) % pages.count)
        return pages[nxt]
    }
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if pendingViewControllers[0] == self.pages[0]{
            NotificationCenter.default.post(name: Notification.Name("get_offset_set_view"), object: 0)
        }else if pendingViewControllers[0] == self.pages[1]{
            NotificationCenter.default.post(name: Notification.Name("get_offset_set_view"), object: UIScreen.main.bounds.width/3)
        }else if pendingViewControllers[0] == self.pages[2]{
            NotificationCenter.default.post(name: Notification.Name("get_offset_set_view"), object: (UIScreen.main.bounds.width/3)*2)
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        if (!completed)
        {
            return
        }
//        print(previousViewControllers)
//        if previousViewControllers[0] != pages[0]{
//
//        }else{
//            NotificationCenter.default.post(name: Notification.Name("get_offset_set_view"), object: UIScreen.main.bounds.width/2)
//        }
    }
   
   
}
