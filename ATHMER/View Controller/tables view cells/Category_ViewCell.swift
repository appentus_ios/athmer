//
//  Category_ViewCell.swift
//  IQKeyboardManager
//
//  Created by Appentus Technologies on 11/20/19.
//

import UIKit

class Category_ViewCell: UITableViewCell {

    @IBOutlet weak var item_image: UIImageView!
    @IBOutlet weak var item_name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
