//
//  Choose_Month_ViewCell.swift
//  ATHMER
///Users/rajatpathak/Desktop/Development/ATHMER/Project/ATHMER/ATHMER/View Controller
//  Created by Appentus Technologies on 11/21/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Choose_Month_ViewCell: UITableViewCell {
    //MARK: All Outlet
    
    @IBOutlet weak var btn_Check_Uncheck: UIButton!
    @IBOutlet weak var Lbl_Month: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization codeUnselect.png
        btn_Check_Uncheck.setImage(UIImage(named: "Unselect.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
        btn_Check_Uncheck.setImage(UIImage(named: "selectfull.png")?.withRenderingMode(.alwaysOriginal), for: .selected)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func btn_check_uncheck(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected

    }
}
