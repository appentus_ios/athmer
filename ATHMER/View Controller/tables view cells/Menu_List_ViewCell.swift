//
//  Menu_List_ViewCell.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/29/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit
import PVSwitch

class Menu_List_ViewCell: UITableViewCell {
    @IBOutlet weak var lbl_itme_name: UILabel!
    @IBOutlet weak var lbl_item_disciption: UILabel!
    @IBOutlet weak var lbl_item_Rate: UILabel!
    @IBOutlet weak var lbl_item_aviability: UILabel!
    @IBOutlet weak var swt_on_off: PVSwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//     switct_status.transform = CGAffineTransform(scaleX: 0.75, y: 0.60)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
