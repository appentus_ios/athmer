//
//  Menu_Offers_ViewCell.swift
//  ATHMER
//
//  Created by appentus on 12/4/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit
import PVSwitch

class Menu_Offers_ViewCell: UITableViewCell {
    @IBOutlet weak var img_item_image: UIImageView!
    @IBOutlet weak var lbl_item_name: UILabel!
    @IBOutlet weak var lbl_item_discription: UILabel!
    @IBOutlet weak var lbl_rate_after_discount: UILabel!
    @IBOutlet weak var lbl_rate_before_discount: UILabel!
    
    @IBOutlet weak var lbl_item_avaibllity: UILabel!
    @IBOutlet var swt_on_off: PVSwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
