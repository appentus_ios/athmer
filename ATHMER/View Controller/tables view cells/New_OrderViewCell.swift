//  New_OrderViewCell.swift
//  ATHMER
//  Created by Appentus Technologies on 11/20/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.



protocol Delegate_Random {
    func func_Delegate_Random(_ cell:New_OrderViewCell)
}



import UIKit

var random_numbers = 0

class New_OrderViewCell: UITableViewCell,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var lbl_orderno: UILabel!
    @IBOutlet weak var lbl_item_name: UILabel!
    @IBOutlet weak var lbl_item_size: UILabel!
    @IBOutlet weak var lbl_item_quantity: UILabel!
    @IBOutlet weak var lbl_item_total: UILabel!
    
    @IBOutlet weak var view_orderlist: UIView!
    
    @IBOutlet weak var btn_delivery_check: UIButton!
    @IBOutlet weak var btn_selfdelivery_check: UIButton!
    @IBOutlet weak var tbl_Add_new_item: UITableView!
    @IBOutlet weak var view_Accept_Order: UIView!
    @IBOutlet weak var btn_accept_order: UIButton!
    @IBOutlet weak var btn_cancel_order: UIButton!
    
    var delegate:Delegate_Random?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tbl_Add_new_item.delegate = self
        tbl_Add_new_item.dataSource = self
        // Initialization code
        
        view_orderlist.layer.cornerRadius = 10
        view_orderlist.layer.shadowColor = UIColor.lightGray.cgColor
        view_orderlist.layer.shadowOpacity = 1
        view_orderlist.layer.shadowRadius = 3
        view_orderlist.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        random_numbers = Int.random(in: 1 ... 15)
        return random_numbers
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! New_item_TableViewCell
        
        delegate?.func_Delegate_Random(self)
        
        return cell2
    }
    
//    MARK: Action
    @IBAction func btn_delivery(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btn_selfdelivery(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func btn_accept_order(_ sender: Any) {
    view_Accept_Order.isHidden = true
    }
    
    
}
