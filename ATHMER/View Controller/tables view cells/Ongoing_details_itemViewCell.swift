//
//  Ongoing_details_itemViewCell.swift
//  ATHMER
//
//  Created by appentus on 12/4/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Ongoing_details_itemViewCell: UITableViewCell {
    @IBOutlet weak var lbl_item_name: UILabel!
    @IBOutlet weak var lbl_qantity: UILabel!
    @IBOutlet weak var lbl_item_size: UILabel!
    @IBOutlet weak var lbl_iitem_price: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
