//
//  Ongoing_item_ViewCell.swift
//  ATHMER
//
//  Created by appentus on 12/3/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Ongoing_item_ViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_itemname: UILabel!
    @IBOutlet weak var lbl_item_size: UILabel!
    @IBOutlet weak var lbl_item_quantity: UILabel!
    @IBOutlet weak var lbl_item_price: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
