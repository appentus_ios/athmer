//
//  Ongoing_order_ViewCell.swift
//  ATHMER
//
//  Created by appentus on 12/3/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Ongoing_order_ViewCell: UITableViewCell,UITableViewDataSource,UITableViewDelegate {
    //MARK: outlet
    
    @IBOutlet weak var lbl_Order_No: UILabel!
    @IBOutlet weak var lbl_Date_Time: UILabel!
    @IBOutlet weak var btn_Cod: Custom_Button_Border!
    @IBOutlet weak var lbl_item_total: UILabel!
    @IBOutlet weak var lbl_item_sub_total: UILabel!
    @IBOutlet weak var tbl_ongoing_item: UITableView!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var btn_View_Details: Custom_Button_Border!
    @IBOutlet weak var lbl_order_preparing: UILabel!
    @IBOutlet weak var btn_order_preparing: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       tbl_ongoing_item.delegate = self
       tbl_ongoing_item.dataSource = self

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "Ongoing_item_ViewCell", for: indexPath) as! Ongoing_item_ViewCell
        return cell2
    }
}
