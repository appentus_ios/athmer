//
//  Post_order_TableViewCell.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/21/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class Post_order_TableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_Order_no: UILabel!
    @IBOutlet weak var btn_delivered: CustomButton!
    @IBOutlet weak var btn_cod: UIButton!
    @IBOutlet weak var lbl_date_time: UILabel!
    @IBOutlet weak var lbl_item_quantity: UILabel!
    @IBOutlet weak var lbl_item_rate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
