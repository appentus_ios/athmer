//
//  ProfileViewCell.swift
//  ATHMER
//
//  Created by Appentus Technologies on 11/25/19.
//  Copyright © 2019 Appentus Technologies. All rights reserved.
//

import UIKit

class ProfileViewCell: UITableViewCell {
    @IBOutlet weak var img_logo: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
